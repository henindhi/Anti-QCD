#ifndef Anti_QCD_antisubjettinesscalc_header
#define Anti_QCD_antisubjettinesscalc_header

#include "Anti_QCD/HCTriggerBase.h"

class AntiSubjettinessCalc : public HCTriggerBase {
   public:
     AntiSubjettinessCalc(std::string name);
     ~AntiSubjettinessCalc();

     int CalculateAntiSubjet (const xAOD::Jet* jet, float& m_tau21_3, float& m_tau21_3_wta, float& m_tau32_1,  float& m_tau32_1_wta) const;
};

#endif
