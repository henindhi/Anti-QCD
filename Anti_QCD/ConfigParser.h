#ifndef Anti_QCD_configparser_header
#define Anti_QCD_configparser_header
#define SAFE_DELETE(T) { if(T) { delete T; T = NULL; } }

#include "TEnv.h"
#include "TString.h"

class ComponentParser{
  public:
    ComponentParser(TEnv& cnfig, const TString& compName, std::string& origin);
    virtual ~ComponentParser();

    std::string varSSName; //better be usual string because later on will be used to fill getAttribute
    TString varSSCutValue; //
    TString varSSAccessType;
    float varSSCastCutValue;
};

class ConfigParser{
  public:
    ConfigParser(const TString& conf_name);
    virtual ~ConfigParser();
    bool Initialize (TEnv& cnfig, std::string& orig);

    const ComponentParser* getComponentValue() const;
    
  private:
    bool m_isInit;
    const TString m_confName;
    ComponentParser* m_compValue;
};



#endif
