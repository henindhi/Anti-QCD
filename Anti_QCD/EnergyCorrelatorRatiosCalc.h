#ifndef Anti_QCD_energycorrelatorratioscalc_header
#define Anti_QCD_energycorrelatorratioscals_header

#include "Anti_QCD/HCTriggerBase.h"


class EnergyCorrelatorRatiosCalc : public HCTriggerBase {
  public:
    EnergyCorrelatorRatiosCalc(std::string name);
    ~EnergyCorrelatorRatiosCalc();

    int CalculateECFRatio (const xAOD::Jet* jet, float& m_c1, float& m_c2, float& m_d2) const ;


};

#endif
