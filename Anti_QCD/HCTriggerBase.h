#ifndef Anti_QCD_hctriggerbase_header
#define Anti_QCD_hctriggerbase_header

#include "xAODCaloEvent/CaloCluster.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

#include "JetRec/JetModifierBase.h"
#include <vector>

class HCTriggerBase {
  public:
    HCTriggerBase(std::string name);
    ~HCTriggerBase();

};
#endif
