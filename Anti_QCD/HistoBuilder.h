#ifndef Anti_QCD_histobuilder_header
#define Anti_QCD_histobuilder_header

#include "TString.h"
#include "TH1I.h"
#include "TEfficiency.h"
#include <vector>

class HistoBuilder{
  public:
    HistoBuilder();
    virtual ~HistoBuilder();

    void MultiplyHisto(TString baseVarName, TString prefix1, TString prefix2, std::vector<std::pair<TH1I*,TH1I*>>& histoVec, Int_t nbins, Double_t xlow, Double_t xup);

    void MultiplyTEff(TString baseVarName, TString prefix1, TString prefix2, std::vector<std::pair<TEfficiency*,TEfficiency*>>& histoVec, Int_t nbins, Double_t xlow, Double_t xup);

    void BuildCheckHisto(TString baseVarName, std::string& prefix, std::vector<TH1F*>& myHisto, size_t& nVar);

    TString teff;
    TString histoName1;
    TString histoName2;
    const char* histoTitle1;
    const char* histoTitle2;
};

#endif
