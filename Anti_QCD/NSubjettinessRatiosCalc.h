#ifndef Anti_QCD_nsubjettinessratioscalc_header
#define Anti_QCD_nsubjettinessratioscalc_header

#include "Anti_QCD/HCTriggerBase.h"


class NSubjettinessRatiosCalc : public HCTriggerBase {
   public:
     NSubjettinessRatiosCalc(std::string name);
     ~NSubjettinessRatiosCalc();

     int CalculateNSubRatio (const xAOD::Jet* jet, float& m_tau21 , float& m_tau21_wta, float& m_tau32,  float& m_tau32_wta) const ;

};

#endif
