#ifndef Anti_QCD_simpletools_header
#define Anti_QCD_simpletools_header

#include "Anti_QCD/HCTriggerBase.h"
#include "TH1I.h"
#include "TLorentzVector.h"

/*#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "TString.h"*/

class SimpleTools {
  public:
    SimpleTools();
    virtual ~SimpleTools();

    //retrieving functions, jet here should be jet->at(n) because "at" somehow doesn't work
    void ValueRetriever(TString& accessType, const xAOD::Jet* jet, std::string& varName, float& floatVarValue, int& intVarValue);
    void ValueRetrieverFloat(TString& accessType, const xAOD::Jet* jet, std::string& varName, float& varValue);
    void ValueRetrieverInt(TString& accessType, const xAOD::Jet* jet, std::string& varName, int& varValue);
    void FourVecRetriever(const xAOD::Jet* jet, std::string& varName, float& FourVecJet);//m, eta
    void p4Retriever(const xAOD::Jet* jet, std::string& varName, float& p4Jet);//Et
    void SSFloatRetriever(const xAOD::Jet* jet, std::string& varName, float& SSFloatJet); //float variable
    void SSIntRetriever(const xAOD::Jet* jet, std::string& varName, int& SSIntJet); //int variable

    //jet ordering function
    void SortTheJet(const xAOD::Jet* jet, size_t& jetIndex, std::string orderBy, double& orderBaseValue, int& leadJet4VecIndex, int& subLeadJet4VecIndex, double& leadJet, double& subLeadJet, int& passSelCounter);
    //jet matching funtion, only for leading and subleading Jet. JetToMatch in this case is the online one, while the target is the offline jet.
    //don't forget to set the jetTargetMatch to jet->at()
    void MatchTheJet(TLorentzVector& jetToMatch_p4, size_t& currIndex, TLorentzVector& targetLdgJet_p4, TLorentzVector& targetSubLdgJet_p4, double& dRmin_LdgJet, double& dRmin_SubLdgJet, int& targetLdgIndex, int& targetSubLdgIndex, int& matchedLdgIndex, int& matchedSubLdgIndex, int& counter);

    //Et finder - sounds like an alien hunter algorithm
    void EtFinder (TH1I*& refHisto, TH1I*& histo, float& EtCut, int& binContent);

    //Simple method to put derived variable calculation into a vector
    void CalcDerived (std::vector<float>& vec, const xAOD::Jet* jet);

    //Overloaded functions for comparison purpose
    bool Comparator (std::string& rule, float& f1, float& f2);
    bool Comparator (std::string& rule, int& f1, int& f2);

    void GetLimUp (int& lost, float& limit);
    void GetLimDown (int& lost, float& limit);
};

#endif
