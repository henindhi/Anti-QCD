#include "Anti_QCD/AntiSubjettinessCalc.h"
#include <math.h>
#include <iostream>


AntiSubjettinessCalc::AntiSubjettinessCalc(std::string name) :
  HCTriggerBase(name)
{

}

AntiSubjettinessCalc::~AntiSubjettinessCalc()
{

}

int AntiSubjettinessCalc::CalculateAntiSubjet(const xAOD::Jet* jet, float& m_tau21_3, float& m_tau21_3_wta, float& m_tau32_1,  float& m_tau32_1_wta) const{
  // Regular
  float tau1 = jet->getAttribute<float>("Tau1");
  float tau2 = jet->getAttribute<float>("Tau2");
  float tau3 = jet->getAttribute<float>("Tau3");

  if(fabs(tau1) > 1e-8) // Prevent div-0
    m_tau32_1 = (tau3+tau2)/tau1;
  else
    m_tau32_1 = -999.0;
  if(fabs(tau3) > 1e-8) // Prevent div-0
    m_tau21_3 = (tau2+tau1)/tau3;
  else
    m_tau21_3 = -999.0;

  float tau1_wta = jet->getAttribute<float>("Tau1_wta");
  float tau2_wta = jet->getAttribute<float>("Tau2_wta");
  float tau3_wta = jet->getAttribute<float>("Tau3_wta");

  // WTA
  if(fabs(tau1_wta) > 1e-8) // Prevent div-0
    m_tau32_1_wta = (tau3_wta + tau2_wta)/tau1_wta;
  else
    m_tau32_1_wta = -999.0;
  if(fabs(tau3_wta) > 1e-8) // Prevent div-0
    m_tau21_3_wta = (tau2_wta + tau1_wta)/tau3_wta;
  else
    m_tau21_3_wta = -999.0;

  return 0;
}
