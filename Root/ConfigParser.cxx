#include "Anti_QCD/ConfigParser.h"
#include <iostream>
#include <string>

//ComponentParser class implementation
ComponentParser::ComponentParser(TEnv& cnfig, const TString& compName, std::string& origin)
{
  varSSName       = cnfig.GetValue(compName+"Name","");
  varSSAccessType = cnfig.GetValue(compName+"Type","");
  if (origin == "left") {
    varSSCutValue   = cnfig.GetValue(compName+"SSCutValueL","");
  } else if (origin == "right") {
    varSSCutValue   = cnfig.GetValue(compName+"SSCutValueR","");
  }
  varSSCastCutValue = varSSCutValue.Atof();

  //const char* confPath = "/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/2015_J_SpecialRepro.config";
  //float testSS = 7;
  //cnfig.SetValue(compName+"Et",testSS);
  //cnfig.WriteFile(confPath,kEnvAll);
  //varSSCastCutValue = std::stof(varSSCutValue);
  //std::cout << "varSSCutValue   : " << varSSCastCutValue << '\n';
  /*
  if (varSSAccessType == "int")
    {
      int varSSCastCutValue = static_cast<int>(varSSCastCutValue);
    }

  std::cout << compName << '\n';
  std::cout << "varSSName       : " << varSSName << '\n';
  std::cout << "varSSCutValue   : " << varSSCastCutValue << '\n';
  std::cout << "varSSAccessType : " << varSSAccessType << '\n';
  std::cout << "---------------------------------------" << '\n';
  */
}

ComponentParser::~ComponentParser()
{

}

//ConfigParser class implementation

ConfigParser::ConfigParser(const TString& conf_name): m_confName(conf_name), m_isInit(false), m_compValue(NULL)
{

}

ConfigParser::~ConfigParser()
{
  SAFE_DELETE(m_compValue);

}

bool ConfigParser::Initialize(TEnv& cnfig, std::string& orig) //cut from left or right
{
  if (m_isInit){
    std::cout << "Oops, already initialized...." << '\n';
    exit(EXIT_FAILURE);
  }

  m_compValue = new ComponentParser(cnfig, m_confName, orig);
  if(!m_compValue){
    std::cout << "Oops, ComponentParser failed...." << '\n';
    exit(EXIT_FAILURE);
  }

  m_isInit = true;
  return 0;
}

const ComponentParser* ConfigParser::getComponentValue() const
{
  if (!m_isInit)
  {
    std::cout << "Oops, you forgot to initialize ConfigParser...." << '\n';
    return NULL;
  }
  return m_compValue;
}
