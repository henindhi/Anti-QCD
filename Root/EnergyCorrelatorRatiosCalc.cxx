#include "Anti_QCD/EnergyCorrelatorRatiosCalc.h"
#include <math.h>


EnergyCorrelatorRatiosCalc::EnergyCorrelatorRatiosCalc(std::string name) : HCTriggerBase(name)
{

}

EnergyCorrelatorRatiosCalc::~EnergyCorrelatorRatiosCalc()
{

}

int EnergyCorrelatorRatiosCalc::CalculateECFRatio (const xAOD::Jet* jet, float& m_c1, float& m_c2, float& m_d2) const {
  float ecf1 = jet->getAttribute<float>("ECF1");
  float ecf2 = jet->getAttribute<float>("ECF2");
  float ecf3 = jet->getAttribute<float>("ECF3");

  // D2
  if(fabs(ecf2) > 1e-8) // Prevent div-0
    m_d2 = ecf3 * pow(ecf1, 3.0) / pow(ecf2, 3.0);
  else
    m_d2 = -999.0;

  // C1
  if(fabs(ecf1) > 1e-8) // Prevent div-0
    m_c1 = ecf2 / pow(ecf1, 2.0);
  else
    m_c1 = -999.0;

  // C2
  if(fabs(ecf2) > 1e-8) // Prevent div-0
    m_c2 = ecf3 * ecf1 / pow(ecf2, 2.0);
  else
    m_c2 = -999.0;

  return 0;
}
