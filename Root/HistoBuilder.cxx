#include "Anti_QCD/HistoBuilder.h"
#include <iostream>

HistoBuilder::HistoBuilder()
{

}

HistoBuilder::~HistoBuilder()
{

}

void HistoBuilder::MultiplyHisto(TString baseVarName, TString prefix1, TString prefix2, std::vector<std::pair<TH1I*,TH1I*>>& histoVec, Int_t nbins, Double_t xlow, Double_t xup)
{
  //histoName1 = Form("%s_%s",baseVarName,prefix1);
  //histoName2 = Form("%s_%s",baseVarName,prefix2);

  histoName1 = baseVarName + prefix1;
  histoName2 = baseVarName + prefix2;
  //std::cout << histoName1 << '\n';
  const char* histoTitle1 = (const char*)histoName1;
  const char* histoTitle2 = (const char*)histoName2;

  TH1I* histoName1 = new TH1I(histoTitle1,histoTitle1,nbins,xlow,xup);
  TH1I* histoName2 = new TH1I(histoTitle2,histoTitle2,nbins,xlow,xup);
  //std::cout << histoName1->GetTitle() << '\n';
  //histoVec.push_back(std::make_pair(histoName1, histoName2));
  histoVec.emplace_back(histoName1,histoName2);
  //std::cout << histoVec.at(0).first->GetTitle() << '\n';
  //std::cout << "filling vector of histo pair,size " << histoVec.size() << '\n';

}
void HistoBuilder::MultiplyTEff(TString baseVarName, TString prefix1, TString prefix2, std::vector<std::pair<TEfficiency*,TEfficiency*>>& histoVec, Int_t nbins, Double_t xlow, Double_t xup)
{
  teff = "_tEff_";
  histoName1 = baseVarName + teff + prefix1;
  histoName2 = baseVarName + teff + prefix2;
  //std::cout << histoName1 << '\n';
  const char* histoTitle1 = (const char*)histoName1;
  const char* histoTitle2 = (const char*)histoName2;

  TEfficiency* histoName1 = new TEfficiency(histoTitle1,histoTitle1,nbins,xlow,xup);
  TEfficiency* histoName2 = new TEfficiency(histoTitle2,histoTitle2,nbins,xlow,xup);
  //std::cout << histoName1->GetTitle() << '\n';
  //histoVec.push_back(std::make_pair(histoName1, histoName2));
  histoVec.emplace_back(histoName1,histoName2);
}

void HistoBuilder::BuildCheckHisto(TString baseVarName, std::string& prefix, std::vector<TH1F*>& myHisto, size_t& nVar){
  TString histoName =  baseVarName + "_check_" + prefix;
  const char* histoTitle = (const char*)histoName;
  if (baseVarName == "D2") {
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,400,0,8);
    myHisto.push_back(histo1);
  }
  else if (baseVarName == "Tau21") {
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,2000,0,2);
    myHisto.push_back(histo1);
  }
  else if (baseVarName == "C2") {
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,500,0,1);
    myHisto.push_back(histo1);
  }
  else if (baseVarName == "Tau32") {
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,2000,0,2);
    myHisto.push_back(histo1);
  }
  else if(baseVarName == "Tau21_3"){
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,150,0,150);
    myHisto.push_back(histo1);
  }
  else if(baseVarName == "Tau21_3_wta"){
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,150,0,150);
    myHisto.push_back(histo1);
  }
  else {
    TH1F* histo1 = new TH1F(histoTitle,histoTitle,1000,0,1);
    myHisto.push_back(histo1);
    myHisto.at(nVar)->GetXaxis()->SetCanExtend(kTRUE);
  }
  // delete histo1;
  //std::cout << "Signal lost histo : " << myHisto.at(nVar)->GetTitle() << '\n';
}
