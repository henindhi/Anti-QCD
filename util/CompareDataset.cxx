//C++ intrinsic function
#include <vector>
#include <iostream>
#include <string>
//GoodRunsList
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
//local dependencies
#include "Anti_QCD/ConfigParser.h"
#include "Anti_QCD/SimpleTools.h"
#include "Anti_QCD/HistoBuilder.h"
//#include "Anti_QCD/HCTriggerBase.h"
#include "Anti_QCD/EnergyCorrelatorRatiosCalc.h"
#include "Anti_QCD/NSubjettinessRatiosCalc.h"
#include "Anti_QCD/AntiSubjettinessCalc.h"

#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "AtlasStyle.C"
#include "AtlasUtils.C"
#include "AtlasLabels.C"
// ROOT includes
#include "TString.h"
#include "TChain.h"
#include "TFile.h"
#include "TEfficiency.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLine.h"
#include "TNamed.h"
#include "TH1F.h"

//#include "TH1I.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

int main (int argc, char* argv[])
{
  xAOD::TEvent event;
  // Check arguments
  if (argc < 3)
    {
      printf("USAGE: %s <output root file> <output pdf file> <is MC?> <input file 1> (input file 2...)\n",argv[0]);
      return 1;
    }

// Get the name of the output file
std::string outRootFile = argv[1];
std::string outPdfFile = argv[2];
std::string isMC = argv[3];
std::string outXAxis = argv[4];

TChain* chain = new TChain("CollectionTree");
for (int iArg = 5; iArg < argc; ++iArg)
  chain->AddFile(argv[iArg]);
if(event.readFrom(chain).isFailure()) exit(1);


//some checks
auto grlSelectionTool = new GoodRunsListSelectionTool("GRLTool");
//std::vector<std::string> grlVec = { "data15_13TeV.periodB1_DetStatus-v62-pro18_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.xml" };
std::vector<std::string> grlVec = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20160720/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"} ;
grlSelectionTool->setProperty("GoodRunsListVec", grlVec);
grlSelectionTool->initialize();

struct varBuilder {
  std::string varBuilderName;
  TString varBuilderType;
};
std::vector<varBuilder> myVarBuilder;

//const char* confPath = "/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/2015_J_SpecialRepro_2JetsCut.config";
const char* confPath = "/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/MCSample.config";
TEnv settings;
if (settings.ReadFile(confPath,kEnvGlobal))
{
  printf("Cannot read config file: %s \n",confPath);
  exit(EXIT_FAILURE);
}

TString myPrefixZPrime = "_ZPrime";
TString myPrefixWPrime = "_WPrime";
TString myPrefixData = "_Data";
TString myPrefix = outPdfFile;
std::string incrementOri = settings.GetValue("IncrementOrigin","");
std::vector<TH1F*> myHisto;

std::string jetOrder = "";
std::string evtInfo = "";
std::string jetContainerinSample = "";
if (outXAxis == "mass") {
  jetOrder = "m";
}
else if (outXAxis == "pt") {
  jetOrder = "pt";
}

if (isMC == "yes") {
  evtInfo = "EventInfo";
  jetContainerinSample = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets";
} else {
  evtInfo = "xEventInfo";
  jetContainerinSample = "MyAntiKt10TrimonlineLCW_calibrated";
}


for (size_t nVar = 0; nVar < 22; nVar++)
{
  const TString confName = Form ("SSComponent.%zu.",nVar);
  ConfigParser parser(confName);

  myVarBuilder.push_back(varBuilder());

  if (parser.Initialize(settings, incrementOri))
  {
    std::cout << "ConfigParser init failed" << '\n';
  }
  else
  {
    const ComponentParser& component = *parser.getComponentValue();
    //fill the value
    myVarBuilder[nVar].varBuilderName = component.varSSName;
    myVarBuilder[nVar].varBuilderType = component.varSSAccessType;

    //build the histogram
    HistoBuilder myHistoBuilder;
    TString histoName =  myVarBuilder[nVar].varBuilderName + myPrefix;
    const char* histoTitle = (const char*)histoName;
    if (myVarBuilder[nVar].varBuilderName == "D2") {
      TH1F* histo1 = new TH1F(histoTitle,histoTitle,100,0,10);
      myHisto.push_back(histo1);
    }
    else if(myVarBuilder[nVar].varBuilderName == "Tau21_3"){
      TH1F* histo1 = new TH1F(histoTitle,histoTitle,100,0,150);
      myHisto.push_back(histo1);
    }
    else if(myVarBuilder[nVar].varBuilderName == "Tau21_3_wta"){
      TH1F* histo1 = new TH1F(histoTitle,histoTitle,100,0,150);
      myHisto.push_back(histo1);
    }
    else {
      TH1F* histo1 = new TH1F(histoTitle,histoTitle,100,0,1);
      myHisto.push_back(histo1);
      myHisto.at(nVar)->GetXaxis()->SetCanExtend(kTRUE);
    }
    std::cout << "Signal lost histo : " << myHisto.at(nVar)->GetTitle() << '\n';
  }
}

bool debugMode = false;
int AvailableEntry = 0;

if (debugMode){
  AvailableEntry = 10000;
}
else {
  AvailableEntry = event.getEntries();
}


size_t derivBoundary = myHisto.size() - 11;
HCTriggerBase hcBase("hcBase");
EnergyCorrelatorRatiosCalc eCorrCalc ("eCorrCalc");
AntiSubjettinessCalc aSubCalc ("aSubCalc");
NSubjettinessRatiosCalc nSubCalc ("nSubCalc");

float etaCut = 2.8;
float massCut = 30.e3;

for (Long64_t iEntry = 0; iEntry < AvailableEntry; ++iEntry)
{
  event.getEntry(iEntry);
  if (iEntry%10000==0)
      printf("Processing entry %lld/%lld\n",iEntry,event.getEntries());

  const xAOD::EventInfo* eInfo = NULL;
  if(event.retrieve(eInfo,evtInfo).isFailure())
  //if(event.retrieve(eInfo,"EventInfo").isFailure())
  {
    printf("Failed to retrieve EventInfo\n");
    exit(1);
  }
  // First retrieve the large-R jets
  const xAOD::JetContainer* trigJets = NULL;
  //if (event.retrieve(trigJets,"AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets").isFailure())
  if (event.retrieve(trigJets,jetContainerinSample).isFailure())
  {
    printf("Failed to retrieve trigger jet collection\n");
    exit(2);
  }

  //SimpleTools
  SimpleTools myTools;
  //for offline ordering
  int firstJetIndex = -1, secondJetIndex = -1, selCounter = 0;
  double firstJet = 0, secondJet = 0, currVal = 0;;

  for (size_t iJet = 0, length = trigJets->size(); iJet < length ; ++iJet){
    if (outXAxis == "mass") {
      // if(fabs(trigJets->at(iJet)->eta()) < 2.0 && trigJets->at(iJet)->pt() > 550.e3)
      // {
        const xAOD::Jet* currJet = trigJets->at(iJet);

        myTools.SortTheJet(currJet, iJet, jetOrder, currVal, firstJetIndex, secondJetIndex, firstJet, secondJet, selCounter);
      // }
    }
    else
    {
      // if(fabs(trigJets->at(iJet)->eta()) < 2.0 && trigJets->at(iJet)->m() > 50.e3)
      // {
        const xAOD::Jet* currJet = trigJets->at(iJet);

        myTools.SortTheJet(currJet, iJet, jetOrder, currVal, firstJetIndex, secondJetIndex, firstJet, secondJet, selCounter);
      // }
    }
  }

  //std::vector<float> derivedVar;
  //std::vector<size_t>::const_iterator it, end(trigJets->end());
  // for (size_t iJet = 0, length = trigJets->size(); iJet < length ; ++iJet) {
    //int counterDerived = 0;

    float currEta = 0, currMass = 0;
    int uselessInt = 0;
    if (firstJetIndex !=-1) {
      //retrieve eta value
      myTools.ValueRetriever(myVarBuilder[0].varBuilderType, trigJets->at(firstJetIndex), myVarBuilder[0].varBuilderName, currEta, uselessInt);
      //std::cout << "current Eta : " << currEta << '\n';
      //retrieve mass value
      myTools.ValueRetriever(myVarBuilder[1].varBuilderType, trigJets->at(firstJetIndex), myVarBuilder[1].varBuilderName, currMass, uselessInt);
      //std::cout << "current Mass : " << currMass << '\n';

    }

    float c1 = 0, c2 = 0, d2 = 0, tau21 = 0, tau32 = 0, tau21_wta = 0, tau32_wta = 0, tau21_3 = 0, tau21_3_wta = 0, tau32_1 = 0, tau32_1_wta = 0;

    for (size_t iVar = 0, length = myHisto.size(); iVar < length; iVar++)
    {
      float floatVar = 0;
      int intVar = 0;
      if (isMC == "yes"
      // && fabs(currEta) < etaCut && currMass > massCut && firstJetIndex !=-1
      ) {
        if (iVar < derivBoundary) {
          myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(firstJetIndex), myVarBuilder[iVar].varBuilderName, floatVar, intVar);
          if (myVarBuilder[iVar].varBuilderType != "int") {
            myHisto.at(iVar)->Fill(floatVar);
          }
          else {
            myHisto.at(iVar)->Fill(intVar);
          }

        } else {
          eCorrCalc.CalculateECFRatio(trigJets->at(firstJetIndex), c1, c2, d2);
          nSubCalc.CalculateNSubRatio(trigJets->at(firstJetIndex), tau21, tau21_wta, tau32, tau32_wta);
          aSubCalc.CalculateAntiSubjet(trigJets->at(firstJetIndex), tau21_3, tau21_3_wta, tau32_1, tau32_1_wta);
          if (iVar == 11 && c1>0) {
            myHisto.at(iVar)->Fill(c1);
          }
          if (iVar == 12 && c2>0) {
            myHisto.at(iVar)->Fill(c2);
          }
          if (iVar == 13 && d2>0 && d2<10) {
            myHisto.at(iVar)->Fill(d2);
          }
          if (iVar == 14 && tau21 >0) {
            myHisto.at(iVar)->Fill(tau21);
          }
          if (iVar == 15 && tau21_wta>0) {
            myHisto.at(iVar)->Fill(tau21_wta);
          }
          if (iVar == 16 && tau32>0) {
            myHisto.at(iVar)->Fill(tau32);
          }
          if (iVar == 17 && tau32_wta>0) {
            myHisto.at(iVar)->Fill(tau32_wta);
          }
          if (iVar == 18 && tau21_3 >0) {
            myHisto.at(iVar)->Fill(tau21_3);
          }
          if (iVar == 19 && tau21_3_wta>0) {
            myHisto.at(iVar)->Fill(tau21_3_wta);
          }
          if (iVar == 20 && tau32_1>0) {
            myHisto.at(iVar)->Fill(tau32_1);
          }
          if (iVar == 21 && tau32_1_wta>0) {
            myHisto.at(iVar)->Fill(tau32_1_wta);
          }
        }
      } else if (isMC == "no"
      // && fabs(currEta) < etaCut && currMass > massCut && firstJetIndex !=-1
      ) {
        myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(firstJetIndex), myVarBuilder[iVar].varBuilderName, floatVar, intVar);
        if (iVar < derivBoundary) {
          if (myVarBuilder[iVar].varBuilderType != "int" && floatVar > -10) {
            // if (myVarBuilder[iVar].varBuilderName == "D2" && floatVar < 10) {
            //   //myHisto.at(iVar)->GetXaxis()->SetRange(0,18);
            //   myHisto.at(iVar)->Fill(floatVar);
            // } else {
              myHisto.at(iVar)->Fill(floatVar);
            // }
          }
          else {
            myHisto.at(iVar)->Fill(intVar);
          }
        } else {
          aSubCalc.CalculateAntiSubjet(trigJets->at(firstJetIndex), tau21_3, tau21_3_wta, tau32_1, tau32_1_wta);
          if (iVar == 18 && tau21_3 >0) {
            myHisto.at(iVar)->Fill(tau21_3);
          }
          if (iVar == 19 && tau21_3_wta>0) {
            myHisto.at(iVar)->Fill(tau21_3_wta);
          }
          if (iVar == 20 && tau32_1>0) {
            myHisto.at(iVar)->Fill(tau32_1);
          }
          if (iVar == 21 && tau32_1_wta>0) {
            myHisto.at(iVar)->Fill(tau32_1_wta);
          }
        }
      }
    }
  // }
}

//TFile* outFileHandle = new TFile(outRootFile.c_str(),"RECREATE");
TFile* outFileHandle = TFile::Open(outRootFile.c_str(), "RECREATE");
outFileHandle->cd();

for (size_t iVar = 0, length = myHisto.size(); iVar < length; iVar++)
{
  Double_t scale = 1/myHisto.at(iVar)->Integral();
  myHisto.at(iVar)->Scale(scale);
  myHisto.at(iVar)->Write();
}
return 0;
}
