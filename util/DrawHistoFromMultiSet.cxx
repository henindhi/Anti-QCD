//C++ standard library
#include <vector>
#include <iostream>
#include <string>
//local dependencies
#include "Anti_QCD/ConfigParser.h"
#include "Anti_QCD/SimpleTools.h"
#include "Anti_QCD/HistoBuilder.h"
//#include "Anti_QCD/HCTriggerBase.h"
#include "Anti_QCD/EnergyCorrelatorRatiosCalc.h"
#include "Anti_QCD/NSubjettinessRatiosCalc.h"
//AtlasStyle
#include "AtlasStyle.C"
#include "AtlasUtils.C"
#include "AtlasLabels.C"
//ROOT includes
#include "TString.h"
#include "TChain.h"
#include "TFile.h"
#include "TEfficiency.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLine.h"
#include "TNamed.h"
#include "TH1F.h"
#include "THStack.h"

int main (int argc, char* argv[])
{
  xAOD::TEvent event;
  // Check arguments
  if (argc < 3)
    {
      printf("USAGE: %s <input root file> <output pdf file>\n",argv[0]);
      return 1;
    }

  const char* inRootFile = argv[1];
  std::string outPdfFile = argv[2];
  TFile* outFileHandle = TFile::Open(inRootFile);

  struct varBuilder {
    TString varBuilderName;
  };
  std::vector<varBuilder> myVarBuilder;

  const char* confPath = "/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/MCSample.config";
  TEnv settings;
  if (settings.ReadFile(confPath,kEnvGlobal))
  {
    printf("Cannot read config file: %s \n",confPath);
    exit(EXIT_FAILURE);
  }

  TString myPrefixZPrime = "_ZPrime";
  TString myPrefixWPrime = "_WPrime";
  TString myPrefixHiggs = "_ggH125";
  TString myPrefixData = "_Data";
  std::string incrementOri = settings.GetValue("IncrementOrigin","");
  TCanvas* canvas = new TCanvas("canvas","canvas");
  const TString outFile1 = outPdfFile;
  canvas->Print(outFile1+"[");

  for (size_t iVar = 0; iVar < 18; iVar++)
  {
    const TString confName = Form ("SSComponent.%zu.",iVar);
    ConfigParser parser(confName);

    myVarBuilder.push_back(varBuilder());

    if (parser.Initialize(settings, incrementOri))
    {
      std::cout << "ConfigParser init failed" << '\n';
    }
    else
    {
      const ComponentParser& component = *parser.getComponentValue();
      //fill the value
      myVarBuilder[iVar].varBuilderName = component.varSSName;
    }

    TString h1Name =  myVarBuilder[iVar].varBuilderName + myPrefixZPrime;
    TString h2Name =  myVarBuilder[iVar].varBuilderName + myPrefixWPrime;
    TString h3Name =  myVarBuilder[iVar].varBuilderName + myPrefixHiggs;
    TString h4Name =  myVarBuilder[iVar].varBuilderName + myPrefixData;

    std::cout << "Histo Name : " << h1Name << " | " << h2Name << " | " << h3Name << " | " << h4Name << '\n';

    TH1F *h1 = (TH1F*)outFileHandle->Get(h1Name);
    TH1F *h2 = (TH1F*)outFileHandle->Get(h2Name);
    TH1F *h3 = (TH1F*)outFileHandle->Get(h3Name);
    TH1F *h4 = (TH1F*)outFileHandle->Get(h4Name);
    h1->SetLineColor(kBlue);
    h2->SetLineColor(kRed);
    h3->SetLineColor(kGreen);
    h4->SetLineColor(kMagenta);

    const char* histoTitle = (const char*)myVarBuilder[iVar].varBuilderName;

    THStack hs("hs","test stacked histograms");
    hs.Add(h1);
    hs.Add(h2);
    hs.Add(h3);
    hs.Add(h4);
    hs.Draw("nostack");
    hs.GetXaxis()->SetTitle(histoTitle);
    hs.GetYaxis()->SetTitle("Arbritary number");
    canvas->Update();

    //const TString confName = Form ("SSComponent.%zu.",iVar);
    TString xPos1Jet = settings.GetValue(confName+"SSCutValue","");
    TString xPos2Jet = settings.GetValue(confName+"SSCutValue2","");
    float xPos1JetCut = xPos1Jet.Atof();
    float xPos2JetCut = xPos2Jet.Atof();
    TLine *oneJetCutLine = new TLine(xPos1JetCut,canvas->GetUymin() ,xPos1JetCut,canvas->GetUymax());
    TLine *twoJetsCutLine = new TLine(xPos2JetCut,canvas->GetUymin() ,xPos2JetCut,canvas->GetUymax());
    oneJetCutLine->SetLineStyle(6);
    twoJetsCutLine->SetLineStyle(9);
    oneJetCutLine->Draw();
    twoJetsCutLine->Draw();

    ATLASLabel(0.6,0.75,"Internal");
    gStyle->SetOptStat(0);
    myBoxText(0.6, 0.70, 0.05, 1, 4, 1 , "ZPrime sample");
    myBoxText(0.6, 0.65, 0.05, 1, 2, 1 , "WPrime sample");
    myBoxText(0.6, 0.60, 0.05, 1, 3, 1 , "Higgs sample");
    myBoxText(0.6, 0.55, 0.05, 1, 6, 1 , "2015 Period J");
    myBoxText(0.6, 0.50, 0.05, 6, 1, 1 , "Cut with 10%% loss in leading Jet");
    myBoxText(0.6, 0.45, 0.05, 9, 1, 1 , "Cut with 10%% loss in sub-leading Jet");

    canvas->Update();
    canvas->Print(outFile1);
    canvas->Clear();
  }
  canvas->Print(outFile1+"]");
  return 0;
}
