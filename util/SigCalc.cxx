//C++ standard library
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
//GoodRunsList
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
//local dependencies
#include "Anti_QCD/ConfigParser.h"
#include "Anti_QCD/SimpleTools.h"
#include "Anti_QCD/HistoBuilder.h"
//#include "Anti_QCD/HCTriggerBase.h"
#include "Anti_QCD/EnergyCorrelatorRatiosCalc.h"
#include "Anti_QCD/NSubjettinessRatiosCalc.h"
#include "Anti_QCD/AntiSubjettinessCalc.h"

#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "AtlasStyle.C"
#include "AtlasUtils.C"
#include "AtlasLabels.C"
// ROOT includes
#include "TString.h"
#include "TChain.h"
#include "TFile.h"
#include "TEfficiency.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLine.h"
//#include "TNamed.h"
//#include "TVector2"

//#include "TH1I.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/TReturnCode.h"

int main (int argc, char* argv[])
{
  SetAtlasStyle();
  xAOD::TEvent event;
  // Check arguments
  if (argc < 6)
    {
      printf("USAGE: %s <output root file> <output pdf file> <what do you want as an x axis> <mode of operation> <Dataset prefix> <isMC> <input file 1> (input file 2...)\n",argv[0]);
      return 1;
    }

// Get the name of the output file
std::string outRootFile = argv[1];
std::string outPdfFile = argv[2];
std::string outXAxis = argv[3];
std::string runOption = argv[4];
std::string setPref = argv[5];
std::string isMC = argv[6];

std::string sigLost = "sigLost";
std::string eqRate = "eqRate";
std::string trigEff = "trigEff";

int mode = 0;

//mode of operation
//mode 1: sigLost, mode 2: eqRate, mode 3: trigEff, mode 5: eqRate+trigEff, mode 6: sigLost+eqRate+trigEff
bool allPointsFound = false, allEqRateFound = false;

if (runOption.find(sigLost) != std::string::npos) {
  mode = mode+1;
}
if (runOption.find(eqRate) != std::string::npos) {
  mode = mode+2;
  if (mode == 2) {
    allPointsFound = true;
  }
}
if (runOption.find(trigEff) != std::string::npos) {
  mode = mode+3;
  if (mode == 3) {
    allEqRateFound = true;
  }
}

//jet order
std::string jetOrder = "";
if (outXAxis == "mass") {
  jetOrder = "m";
}
else if (outXAxis == "pt"){
  jetOrder = "pt";
}

std::string evtInfo = "";
std::string jetContainerOnline = "";
std::string jetContainerOffline = "";

//container name
if (isMC == "yes") {
  evtInfo = "EventInfo";
  jetContainerOnline = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets";
  jetContainerOffline = "AntiKt10LCTopoJets";
  //jetContainerOffline = "AntiKt10TruthTrimmedPtFrac5SmallR20Jets";
} else if (isMC == "no") {
  evtInfo = "EventInfo";
  jetContainerOnline = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets";
  jetContainerOffline = "AntiKt10LCTopoJets";
  //jetContainerOnline = "MyAntiKt10TrimonlineLCW_calibrated";
  //jetContainerOffline = "MyAntiKt10TrimofflineLCW_calibrated" ;
}

std::cout << "--------------------------------------" << '\n';
std::cout << "Mode of operation : " << mode << '\n';
std::cout << "--------------------------------------" << '\n';

TChain* chain = new TChain("CollectionTree");
for (int iArg = 7; iArg < argc; ++iArg)
  chain->AddFile(argv[iArg]);
if(event.readFrom(chain).isFailure()) exit(1);

//some checks
auto grlSelectionTool = new GoodRunsListSelectionTool("GRLTool");
std::vector<std::string> grlVec = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"} ;
///cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20160720/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml
grlSelectionTool->setProperty("GoodRunsListVec", grlVec);
grlSelectionTool->initialize();

//prepare a vector of structs as a vessel for parsed variables
struct varBuilder {
  std::string varBuilderName;
  TString varBuilderType;
  float varBuilderCut;
};
std::vector<varBuilder> myVarBuilder;

//config parser
// const char* confPath = "/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/x00.config";
//
// TEnv settings2, settings;
// if (settings2.ReadFile(confPath,kEnvGlobal))
// {
//   printf("Cannot read config file: %s \n",confPath);
//   exit(EXIT_FAILURE);
// }
//other options parser
//std::string incOri = settings2.GetValue("IncrementOrigin","");
//const char* configPath = settings2.GetValue("ConfPath","");

const char* configPath = "/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/configAllVar/x01.config";
std::cout << "config : " << configPath << '\n';
TEnv settings;
if (settings.ReadFile(configPath,kEnvGlobal))
{
  printf("Cannot read config file: %s \n",configPath);
  exit(EXIT_FAILURE);
}
std::string incOri = settings.GetValue("IncrementOrigin","");
const char* nSgLost = settings.GetValue("SignalLost","");
const char* nJtCut = settings.GetValue("NumJetCut","");
int nSigLost = atoi(nSgLost);
int nJetCut = atoi(nJtCut);

//vector of histos
std::vector<TH1F*> myHistoCheck;
std::vector<std::pair<TH1I*,TH1I*>> myHistoVec;
std::vector<std::pair<TH1I*,TH1I*>> myHistoVecEqRate;
std::vector<std::pair<TEfficiency*,TEfficiency*>> myHistoVecEff;

TString myPrefix1 = "_1JetPass";
TString myPrefix2 = "_2JetPass";
TString myPrefixEq1 = "_EqRate_1JetPass";
TString myPrefixEq2 = "_Eqrate_2JetPass";

std::string numVar = settings.GetValue("NumVariables","");
size_t numVars = std::stoi(numVar,0);

//build the histogram
for (size_t nVar = 0; nVar < numVars; nVar++)
{
  const TString confName = Form ("SSComponent.%zu.",nVar);
  ConfigParser parser(confName);

  myVarBuilder.push_back(varBuilder());

  if (parser.Initialize(settings, incOri))
  {
    std::cout << "ConfigParser init failed" << '\n';
  }
  else
  {
    const ComponentParser& component = *parser.getComponentValue();
    //fill the value
    myVarBuilder[nVar].varBuilderName = component.varSSName;
    myVarBuilder[nVar].varBuilderType = component.varSSAccessType;
    myVarBuilder[nVar].varBuilderCut = component.varSSCastCutValue;

    //build the histogram
    HistoBuilder myHistoBuilder;

    myHistoBuilder.BuildCheckHisto(myVarBuilder[nVar].varBuilderName, setPref, myHistoCheck, nVar);

    myHistoBuilder.MultiplyHisto(myVarBuilder[nVar].varBuilderName, myPrefix1, myPrefix2, myHistoVec, 100, 0, 1);
    myHistoVec.at(nVar).first->GetXaxis()->SetCanExtend(kTRUE);
    myHistoVec.at(nVar).second->GetXaxis()->SetCanExtend(kTRUE);

    myHistoBuilder.MultiplyHisto(myVarBuilder[nVar].varBuilderName, myPrefixEq1, myPrefixEq2, myHistoVecEqRate, 280, 200, 480);

    if (outXAxis == "mass") {
      myHistoBuilder.MultiplyTEff(myVarBuilder[nVar].varBuilderName, myPrefix1, myPrefix2, myHistoVecEff, 50, 0, 100);
    }
    else if (outXAxis == "pt") {
      myHistoBuilder.MultiplyTEff(myVarBuilder[nVar].varBuilderName, myPrefix1, myPrefix2, myHistoVecEff, 35, 150, 600);
    }
  }
}

std::vector<int> passArray(myHistoCheck.size());
std::vector<float> foundedCutValue(myHistoCheck.size());
std::vector<float> lastBinsArray(myHistoCheck.size());
std::vector<int> lastBinsCheckValue(myHistoCheck.size());
std::vector<float> tmpVal(myHistoCheck.size(),0.0);

float EtCut1 = 0, EtCut2 = 0;
int BinContent1 = 0, BinContent2 = 0;
//------------------------------------------------------------------
//Init finished
//------------------------------------------------------------------
std::cout << "size of vector of histo : " << myHistoVec.size() << '\n';
bool debugMode = false;
int AvailableEntry = 0;

if (debugMode){
  AvailableEntry = 10000;
}
else{
  AvailableEntry = event.getEntries();
}

HCTriggerBase hcBase("hcBase");
EnergyCorrelatorRatiosCalc eCorrCalc ("eCorrCalc");
AntiSubjettinessCalc aSubCalc ("aSubCalc");
NSubjettinessRatiosCalc nSubCalc ("nSubCalc");
size_t derivBoundary = myHistoCheck.size() - 11;

//------------------------------------------------------------------
//execute
//------------------------------------------------------------------
int stepTracker = 0;
std::cout << "Step tracker initial : " << stepTracker << '\n';
int yugeEntryCounter = 0; //with Trump's face
//bool lastEntry = false;
while (stepTracker != mode) {
  for (Long64_t iEntry = 0; iEntry < AvailableEntry; ++iEntry)
  {
    event.getEntry(iEntry);
    if (iEntry%10000==0)
        printf("Processing entry %lld/%lld\n",iEntry,event.getEntries());

    const xAOD::EventInfo* eInfo = NULL;

    //if(event.retrieve(eInfo,"xEventInfo").isFailure())
    if(event.retrieve(eInfo,evtInfo).isFailure()){
      printf("Failed to retrieve EventInfo\n");
      exit(1);
    }
    // Retrieve the large-R jet collections
    const xAOD::JetContainer* trigJets = NULL;
    const xAOD::JetContainer* offJets  = NULL;
    const xAOD::TruthParticleContainer* truthParticles = NULL;
    if (event.retrieve(trigJets,jetContainerOnline).isFailure())
    {
      printf("Failed to retrieve trigger jet collection\n");
      exit(2);
    }
    if (event.retrieve(offJets,jetContainerOffline).isFailure())
    {
      printf("Failed to retrieve offline jet collection\n");
      exit(3);
    }
    if (setPref == "higgs" && event.retrieve(truthParticles,"TruthParticles").isFailure())
    {
      printf("Failed to retrieve truth jet collection\n");
      exit(3);
    }

    //SimpleTools
    SimpleTools myTools;

    //for online ordering
    int firstJetIndex = -1, secondJetIndex = -1, selCounter = 0;
    double firstJet = 0, secondJet = 0, currVal = 0;

    //for offline ordering
    int firstOffJetIndex = -1, secondOffJetIndex = -1, selOffCounter = 0;
    double firstOffJet = 0, secondOffJet = 0;

    //for matching
    int matchedLdgIndex = -1, matchedSubLdgIndex =-1;

    //additional
    bool passPreSel = false, passHiggs = 0;

    //Jet ordering and matching
    //std::cout << "Ordering and matching the jets..." << '\n';
    for (size_t i = 0; i < 3; i++) {
      //order the online jets
      if (i == 0) {
        // std::cout << "1st step: ordering online jets" << '\n';
        for (size_t iJet = 0, length = trigJets->size(); iJet < length ; ++iJet){
          if(fabs(trigJets->at(iJet)->eta()) < 2.8)
          {
            const xAOD::Jet* currJet = trigJets->at(iJet);

            myTools.SortTheJet(currJet, iJet, jetOrder, currVal, firstJetIndex, secondJetIndex, firstJet, secondJet, selCounter);
          }
        }
      }

      //order the offline jets plus a little bit of pre-selection
      if (i == 1) {
        for (size_t iJet = 0, length = offJets->size(); iJet < length ; ++iJet){
          if (outXAxis == "mass") {
            if(fabs(offJets->at(iJet)->eta()) < 2.0 && offJets->at(iJet)->pt() > 550.e3)
            {
              const xAOD::Jet* currJet = offJets->at(iJet);

              myTools.SortTheJet(currJet, iJet, jetOrder, currVal, firstOffJetIndex, secondOffJetIndex, firstOffJet, secondOffJet, selOffCounter);
            }
          }
          else if (outXAxis == "pt")
          {
            // std::cout << "Sorting by pt" << '\n';
            if(fabs(offJets->at(iJet)->eta()) < 2.0 && offJets->at(iJet)->m() > 50.e3)
            {
              const xAOD::Jet* currJet = offJets->at(iJet);
              // std::cout << "Before sorting" << '\n';
              myTools.SortTheJet(currJet, iJet, jetOrder, currVal, firstOffJetIndex, secondOffJetIndex, firstOffJet, secondOffJet, selOffCounter);
            }
          }
        }
        if (selOffCounter >= 1) {
          passPreSel = true;
        }
      }

      //match online to offline
      if (i == 2 && passPreSel == true) {
        // std::cout << "3rd step: matching online and offline jets" << '\n';
        TLorentzVector targetLdgJet_p4, targetSubLdgJet_p4, targLdgJet_p4, targSubLdgJet_p4;
        double dRmin_LdgJet=100000, dRmin_SubLdgJet=100000;

        if (selOffCounter >= 1 /*&& firstOffJetIndex != -1*/) {
          if (setPref == "higgs") {
            targLdgJet_p4 = offJets->at(firstOffJetIndex)->p4();
            for (size_t i = 0, length = truthParticles->size(); i < length; i++) {
              const xAOD::TruthParticle* currTruth = truthParticles->at(i);
              if (currTruth->isHiggs()) {
                TLorentzVector currP4 = currTruth->p4();
                int pdgid  = currTruth->pdgId();
                float dR = targLdgJet_p4.DeltaR(currP4);
                if (dR < 1.0 && pdgid == 25) {
                  targetLdgJet_p4 = offJets->at(firstOffJetIndex)->p4();
                  passHiggs++;
                  break;
                }
              }
            }
          }
          else {
            targetLdgJet_p4 = offJets->at(firstOffJetIndex)->p4();
          }
        }
        if (selOffCounter > 1 /*&& secondOffJetIndex != -1*/) {
          targSubLdgJet_p4 = offJets->at(secondOffJetIndex)->p4();
          if (setPref == "higgs") {
            for (size_t i = 0, length = truthParticles->size(); i < length; i++) {
              const xAOD::TruthParticle* currTruth = truthParticles->at(i);
              if (currTruth->isHiggs()) {
                TLorentzVector currP4 = currTruth->p4();
                int pdgid  = currTruth->pdgId();
                float dR = targSubLdgJet_p4.DeltaR(currP4);
                if (dR < 1.0 && pdgid == 25) {
                  targetSubLdgJet_p4 = offJets->at(secondOffJetIndex)->p4();
                  passHiggs++;
                  break;
                }
              }
            }
          }
          else {
            targetSubLdgJet_p4 = offJets->at(secondOffJetIndex)->p4();
          }
        }

        for (size_t iJet = 0, length = trigJets->size(); iJet < length ; ++iJet) {
          TLorentzVector jetToMatch_p4 = trigJets->at(iJet)->p4();

          myTools.MatchTheJet(jetToMatch_p4, iJet, targetLdgJet_p4, targetSubLdgJet_p4, dRmin_LdgJet, dRmin_SubLdgJet, firstOffJetIndex, secondOffJetIndex, matchedLdgIndex, matchedSubLdgIndex, selOffCounter);
        }
      }
    }

    //execute once at the beginning of the run, just to fill the check histogram & get the binning. Make sure that your dataset has at least 10000 events
    if (yugeEntryCounter == 0 && runOption.find(sigLost) != std::string::npos && iEntry < 10000) {
      if (iEntry == 0) {
        std::cout << "starting loop of checkHisto" << '\n';
      }
      float c1 = 0, c2 = 0, d2 = 0, tau21 = 0, tau32 = 0, tau21_wta = 0, tau32_wta = 0, tau21_3 = 0, tau21_3_wta = 0, tau32_1 = 0, tau32_1_wta = 0;

      for (size_t iVar = 0, length = myHistoCheck.size(); iVar < length; iVar++)
      {
        float floatVar = 0;
        int intVar = 0;
        if(secondJetIndex != -1){
          if (iVar < derivBoundary) {
            if (myVarBuilder[iVar].varBuilderType != "int") {
              myTools.ValueRetrieverFloat(myVarBuilder[iVar].varBuilderType, trigJets->at(secondJetIndex), myVarBuilder[iVar].varBuilderName, floatVar);
              myHistoCheck.at(iVar)->Fill(floatVar);
            }
            else {
              myTools.ValueRetrieverInt(myVarBuilder[iVar].varBuilderType, trigJets->at(secondJetIndex), myVarBuilder[iVar].varBuilderName, intVar);
              myHistoCheck.at(iVar)->Fill(intVar);
            }
          }
          else {
            eCorrCalc.CalculateECFRatio(trigJets->at(secondJetIndex), c1, c2, d2);
            nSubCalc.CalculateNSubRatio(trigJets->at(secondJetIndex), tau21, tau21_wta, tau32, tau32_wta);
            aSubCalc.CalculateAntiSubjet(trigJets->at(secondJetIndex), tau21_3, tau21_3_wta, tau32_1, tau32_1_wta);
            //std::cout << "C1 : " << c1 << '\n';
            if (iVar == derivBoundary && c1>0) {
              myHistoCheck.at(iVar)->Fill(c1);
            }
            if (iVar == derivBoundary+1 && c2>0) {
              myHistoCheck.at(iVar)->Fill(c2);
            }
            if (iVar == derivBoundary+2 && d2>0 && d2<10) {
              myHistoCheck.at(iVar)->Fill(d2);
            }
            if (iVar == derivBoundary+3 && tau21 >0 && tau21<8) {
              myHistoCheck.at(iVar)->Fill(tau21);
            }
            if (iVar == derivBoundary+4 && tau21_wta>0) {
              myHistoCheck.at(iVar)->Fill(tau21_wta);
            }
            if (iVar == derivBoundary+5 && tau32>0) {
              myHistoCheck.at(iVar)->Fill(tau32);
            }
            if (iVar == derivBoundary+6 && tau32_wta>0) {
              myHistoCheck.at(iVar)->Fill(tau32_wta);
            }
            if (iVar == derivBoundary+7 && tau21_3 >0) {
              myHistoCheck.at(iVar)->Fill(tau21_3);
            }
            if (iVar == derivBoundary+8 && tau21_3_wta>0) {
              myHistoCheck.at(iVar)->Fill(tau21_3_wta);
            }
            if (iVar == derivBoundary+9 && tau32_1>0) {
              myHistoCheck.at(iVar)->Fill(tau32_1);
            }
            if (iVar == derivBoundary+10 && tau32_1_wta>0) {
              myHistoCheck.at(iVar)->Fill(tau32_1_wta);
            }
          }
        }
        if (iEntry == 9999) {
          int lastBinValue = myHistoCheck.at(iVar)->FindLastBinAbove(4,1);
          lastBinsCheckValue.at(iVar) = lastBinValue;
          lastBinsArray.at(iVar) = myHistoCheck.at(iVar)->GetXaxis()->GetBinCenter(lastBinValue);
          std::cout << "last bin of " << myVarBuilder[iVar].varBuilderName << " with meaningful value : " << lastBinsArray.at(iVar) << ", with a bin number : " << lastBinValue <<  '\n';
        }
      }
      if (iEntry == 9999) {
      yugeEntryCounter++;
      }
    }

    //-------------------------------------------------------------
    //Signal lost
    //-------------------------------------------------------------

    ///*
    if (yugeEntryCounter > 0 && runOption.find(sigLost) != std::string::npos && allPointsFound == false) {
      int counterDerived1 = 0, counterDerived2 = 0;
      bool at_least_2jets = false;
      float entryWEtaM1 = 0;
      float entryWEtaM2 = 0;
      int overflowMassBin = 0;
      for (size_t iVar = 0, length = myHistoVec.size(); iVar < length; iVar++)
      {
        if (myVarBuilder[iVar].varBuilderName != "m" && passArray.at(iVar) == 1) {
          continue;
        }

        float f_var1 = 0, f_var2 = 0;
        int i_var1 = 0, i_var2 = 0;
        float massCut = myVarBuilder[1].varBuilderCut;

        int binN = 0;
        // if (iEntry == 0) {
        //   lastBinsSigArray.at(iVar) = myHistoVec.at(iVar).first->GetXaxis()->FindBin(lastBinsArray.at(iVar));
        //   //std::cout << "Last bin in sig histo " << myVarBuilder[iVar].varBuilderName << " is " << lastBinsSigArray.at(iVar) << '\n';
        // }
        // int lastBin = lastBinsSigArray.at(iVar);
        if (incOri == "left") {
          binN = yugeEntryCounter;
        }
        else if (incOri == "right"){
          binN = lastBinsCheckValue.at(iVar) - yugeEntryCounter;
        }
        float varCut = myHistoCheck.at(iVar)->GetXaxis()->GetBinLowEdge(binN);

        float massLead = 0, massSubLead = 0;

        std::vector<float> derivedVar1;
        std::vector<float> derivedVar2;

        if (selCounter >= 1 /*&& firstJetIndex != -1*/) {
          massLead = trigJets->at(firstJetIndex)->m();
          if (iVar < derivBoundary) {
            myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(firstJetIndex), myVarBuilder[iVar].varBuilderName, f_var1, i_var1);
            if (selCounter > 1 /*&& secondJetIndex != -1*/) {
              massSubLead = trigJets->at(secondJetIndex)->m();
              myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(secondJetIndex), myVarBuilder[iVar].varBuilderName, f_var2, i_var2);
              at_least_2jets = true;
            }
          } else /*if (iVar >= derivBoundary)*/ {
            myTools.CalcDerived(derivedVar1, trigJets->at(firstJetIndex));
            if (at_least_2jets) {
              massSubLead = trigJets->at(secondJetIndex)->m();
              myTools.CalcDerived(derivedVar2, trigJets->at(secondJetIndex));
            }
          }
          //std::cout << "variable value : " << myVarBuilder[iVar].varBuilderName << f_var1 << " | " << i_var1  << '\n';

          //retrieve value for leading jet, non-int type
          if (myVarBuilder[iVar].varBuilderType != "int")
          {
            if (myVarBuilder[iVar].varBuilderName == "eta")
            {
              //std::cout << "Filling eta...." << '\n';
              myHistoVec.at(iVar).first->Fill(f_var1);
              if (at_least_2jets) {
                myHistoVec.at(iVar).second->Fill(f_var2);
              }
            }
            else if (myVarBuilder[iVar].varBuilderName == "m" && massLead > massCut)
            {
              myHistoVec.at(iVar).first->Fill(f_var1);
              if (at_least_2jets && massSubLead > massCut) {
                myHistoVec.at(iVar).second->Fill(f_var2);
              }
            }
            else
            {
              if (iVar < derivBoundary) {
                //BEWARE of the sign of varcut after this line
                if (massLead > massCut && myTools.Comparator(incOri, f_var1, varCut)) //f_var1 < varCut)//SIGN
                {
                  myHistoVec.at(iVar).first->Fill(f_var1);
                  if (at_least_2jets && massSubLead > massCut && myTools.Comparator(incOri, f_var2, varCut)) {//SIGN
                    myHistoVec.at(iVar).second->Fill(f_var2);
                  }
                }
              }
              else{
                if (massLead > massCut && myTools.Comparator(incOri, derivedVar1.at(counterDerived1), varCut)) // derivedVar1.at(counterDerived1) < varCut)//SIGN
                {
                  myHistoVec.at(iVar).first->Fill(derivedVar1.at(counterDerived1));

                  if (at_least_2jets && massSubLead > massCut && myTools.Comparator(incOri, derivedVar2.at(counterDerived2), varCut)) {//SIGN
                    myHistoVec.at(iVar).second->Fill(derivedVar2.at(counterDerived2));
                    counterDerived2++;
                  }
                  counterDerived1++;
                }
              }
            }
          }
          //retrieveing value for int variable
          else
          {
            int varIntCut = static_cast<int>(varCut);
            if (massLead > massCut && myTools.Comparator(incOri, i_var1, varIntCut)) //i_var1 < varIntCut)//SIGN
            {
              myHistoVec.at(iVar).first->Fill(i_var1);
              if (at_least_2jets && massSubLead > massCut && myTools.Comparator(incOri, i_var2, varIntCut)) {//SIGN
                myHistoVec.at(iVar).second->Fill(i_var2);
              }
            }
          }
          //BEWARE varcut sign controversy is over
        }
        derivedVar1.clear();
        derivedVar2.clear();

        if (iEntry == AvailableEntry - 1) {
          std::cout << "Bin " << binN << " of " << myVarBuilder[iVar].varBuilderName << " with bin edge " << varCut << '\n';

          if (iVar == 1) {
            overflowMassBin = myHistoVec.at(1).first->GetSize() - 1;
            //change this in case you want to compare with the plot with only eta cut
            //float entryWEta1 = myHistoVec.at(0).first->GetEntries();
            //float entryWEta2 = myHistoVec.at(0).second->GetEntries();
            entryWEtaM1 = myHistoVec.at(1).first->Integral(0,overflowMassBin,"");
            entryWEtaM2 = myHistoVec.at(1).second->Integral(0,overflowMassBin,"");

            //std::cout << "entry with mass : " << entryWEtaM1 << " | " << entryWEtaM2 << '\n';
            myHistoVec.at(1).first->Reset();
            myHistoVec.at(1).second->Reset();
          }
          int overflowSSBin = myHistoVec.at(iVar).first->GetSize() - 1;

          float entryWEtaMSS1 = myHistoVec.at(iVar).first->Integral(0,overflowSSBin,"");
          entryWEtaMSS1 = (entryWEtaMSS1*100)/entryWEtaM1;
          float entryWEtaMSS2 = myHistoVec.at(iVar).second->Integral(0,overflowSSBin,"");
            // std::cout << "SS2 up : " << entryWEtaMSS2 << " and down : " << entryWEtaM2 << '\n';
          entryWEtaMSS2 = (entryWEtaMSS2*100)/entryWEtaM2;

          myHistoVec.at(iVar).first->Reset();
          myHistoVec.at(iVar).second->Reset();
          passArray.at(0) = 1;
          passArray.at(1) = 1;
          std::cout << "signal remaining, eta+mass+" << myVarBuilder[iVar].varBuilderName << " 1 jet cut :" << entryWEtaMSS1 << " | eta+mass+" << myVarBuilder[iVar].varBuilderName << " 2 jet cut :" << entryWEtaMSS2 << '\n';

          float ntry = 0, upperLim = 0, lowerLim = 0;
          if (nJetCut == 1) {
            ntry = entryWEtaMSS1;
          } else {
            ntry = entryWEtaMSS2;
          }
          myTools.GetLimUp(nSigLost, upperLim);
          myTools.GetLimDown(nSigLost, lowerLim);

          TString compName = Form ("SSComponent.%zu.",iVar);
          TString sigRem = "";
          if (incOri == "left") {
            sigRem = compName+"SigRemL";
            compName = compName+"SSCutValueL";
          }
          else if (incOri == "right") {
            sigRem = compName+"SigRemR";
            compName = compName+"SSCutValueR";
          }

          if (iVar > 1 && ntry < upperLim && ntry > lowerLim) {
            //alter the array value & save the value
            settings.SetValue(compName,varCut);
            settings.SetValue(sigRem,ntry);
            settings.WriteFile(configPath,kEnvAll);

            passArray.at(iVar) = 1;
            std::cout << "passArray : " << myVarBuilder[iVar].varBuilderName <<  " | " << passArray.at(iVar) << " | " << varCut << '\n';
            foundedCutValue.at(iVar) = varCut;
          }
          else if(iVar > 1 && ntry > upperLim){
            tmpVal.at(iVar) = ntry;
          }
          else if(iVar > 1 && ntry < lowerLim && tmpVal.at(iVar) == 0){
            settings.SetValue(compName,varCut);
            settings.SetValue(sigRem,ntry);
            settings.WriteFile(configPath,kEnvAll);

            passArray.at(iVar) = 1;
            std::cout << "passArray : " << myVarBuilder[iVar].varBuilderName <<  " | " << passArray.at(iVar) << " | " << varCut << '\n';
            foundedCutValue.at(iVar) = varCut;
          }
          else if(iVar > 1 && ntry < lowerLim && tmpVal.at(iVar) > 0){
            float dUp = tmpVal.at(iVar) - upperLim;
            float dLow = lowerLim - ntry;
            if (dLow < dUp) {
              settings.SetValue(compName,varCut);
              settings.SetValue(sigRem,ntry);
              settings.WriteFile(configPath,kEnvAll);
              passArray.at(iVar) = 1;
              std::cout << "passArray : " << myVarBuilder[iVar].varBuilderName <<  " | " << passArray.at(iVar) << " | " << varCut << '\n';
              foundedCutValue.at(iVar) = varCut;
            }
            else{
              settings.SetValue(compName,varCut);
              settings.SetValue(sigRem,tmpVal.at(iVar));
              settings.WriteFile(configPath,kEnvAll);
              passArray.at(iVar) = 1;
              std::cout << "passArray : " << myVarBuilder[iVar].varBuilderName <<  " | " << passArray.at(iVar) << " | " << varCut << '\n';
              foundedCutValue.at(iVar) = varCut;
            }
          }
        }
      }
      //check the array value
      if (iEntry == AvailableEntry - 1) {
        std::cout << "Entry loop counter : " << yugeEntryCounter << '\n';
        yugeEntryCounter++;

        if (std::all_of(passArray.cbegin(), passArray.cend(), [](int i){ return i == 1;})) {
          std::cout << "Found all points" << '\n';
          allPointsFound = true;
          stepTracker = 1;
        }
      }
    }//*/

    //--------------------------------------------------------------
    //Equal rate
    //-------------------------------------------------------------
    if (runOption.find(eqRate) != std::string::npos && allPointsFound == true) {
      // For-Loop: All ET bins
      for (int binX = 1, length = myHistoVecEqRate.at(0).first->GetNbinsX(); binX <= length; ++binX)
      {
        // Get the value of the edge
        const double binEdge = myHistoVecEqRate.at(0).first->GetXaxis()->GetBinLowEdge(binX)*1.0e3;
        // Loop through the variable selection
        for (size_t iVar = 0, length =  myHistoVecEqRate.size(); iVar < length; iVar++) {
          float currEt = 0, currMass = 0, f_var3 = 0;
          int i_var3 = 0, eqRateCounter = 0;;

          TString etType = "p4", massType = "4Vec";
          std::string etName = "Et", massName = "m";

          float varCut = myVarBuilder[iVar].varBuilderCut;
          //std::cout << "Cut for " << myVarBuilder[iVar].varBuilderName << " with value of " << varCut << '\n';
          float massCut = myVarBuilder[1].varBuilderCut;
          //loop through the jet
          for (size_t iJet = 0, length = trigJets->size(); iJet < length; ++iJet)
          {
            //retrieve Et value for current jet
            myTools.ValueRetrieverFloat(etType, trigJets->at(iJet), etName, currEt);
            //retrieve mass value for current jet
            myTools.ValueRetrieverFloat(massType, trigJets->at(iJet), massName, currMass);
            //retrieve current SS value
            myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(iJet), myVarBuilder[iVar].varBuilderName, f_var3, i_var3);

            // Perform the selection
            // safe guard against failed eta pre-selection
            if(fabs(trigJets->at(iJet)->eta()) < 2.8){
              if (myVarBuilder[iVar].varBuilderType != "int")
              {
                if (myVarBuilder[iVar].varBuilderName == "eta" && currEt > binEdge)
                {
                  eqRateCounter++;
                }
                else if (myVarBuilder[iVar].varBuilderName == "m" && currEt > binEdge && currMass > massCut) {
                  eqRateCounter++;
                }
                else if (currEt > binEdge && currMass > massCut && myTools.Comparator(incOri, f_var3, varCut)) //f_var3 < varCut)
                {
                  eqRateCounter++;
                }
              }
              else
              {
                int varIntCut = static_cast<int>(varCut);
                if (currEt > binEdge && currMass > massCut && myTools.Comparator(incOri, i_var3, varIntCut)) {
                  eqRateCounter++;
                }
              }
            }
          }

          //Filling time!
          if (eqRateCounter >= 1)
          {
            myHistoVecEqRate.at(iVar).first->Fill(myHistoVecEqRate.at(iVar).first->GetXaxis()->GetBinCenter(binX));
          }
          if (eqRateCounter > 1)
          {
            myHistoVecEqRate.at(iVar).second->Fill(myHistoVecEqRate.at(iVar).second->GetXaxis()->GetBinCenter(binX));
          }
        }
      }
      //mark the end of the loop of entries
      if (iEntry == AvailableEntry - 1) {
        stepTracker = stepTracker + 2;
        //find the Et line
        for (size_t iVar = 0, length = myHistoVec.size(); iVar < length; iVar++){
          const TString compName = Form ("SSComponent.%zu.",iVar);
          //float EtCut1 = 0, EtCut2 = 0;
          //int BinContent = 0;
          //SimpleTools myTools2;
          myTools.EtFinder(myHistoVecEqRate.at(0).first, myHistoVecEqRate.at(iVar).first, EtCut1, BinContent1);
          //std::cout << "bin content etFinder 1 jet : " << BinContent << " , found Et : " << EtCut1 << '\n';
          myTools.EtFinder(myHistoVecEqRate.at(0).first, myHistoVecEqRate.at(iVar).second, EtCut2, BinContent2);
          //std::cout << "bin content etFinder 2 jet : " << BinContent <<  " , found Et : " << EtCut2 << '\n';

          EtCut1 = EtCut1*1.e3;
          EtCut2 = EtCut2*1.e3;

          if (incOri == "left") {
            settings.SetValue(compName+"Et1JetCutL",EtCut1);
            settings.SetValue(compName+"Et2JetCutL",EtCut2);
          }
          else if (incOri == "right"){
            settings.SetValue(compName+"Et1JetCutR",EtCut1);
            settings.SetValue(compName+"Et2JetCutR",EtCut2);
          }

          settings.WriteFile(configPath,kEnvAll);
        }
        allEqRateFound = true;
      }
    }

    //--------------------------------------------------------------
    //Efficiency plot
    //--------------------------------------------------------------
    if (runOption.find(trigEff) != std::string::npos && allEqRateFound == true) {
      if (passPreSel == true) {
        for (size_t iVar = 0, length = myHistoVec.size(); iVar < length; iVar++)
        {
          float f_var1 = 0, f_var2 = 0;
          int i_var1 = 0, i_var2 = 0;
          TString etString1 = "", etString2 = "";
          float massCut = myVarBuilder[1].varBuilderCut;
          float varCut = myVarBuilder[iVar].varBuilderCut;

          const TString compName = Form ("SSComponent.%zu.",iVar);
          if (incOri == "left") {
            etString1 = settings.GetValue(compName+"Et1JetCutL","");
            etString2 = settings.GetValue(compName+"Et2JetCutL","");
          }
          else if (incOri == "right"){
            etString1 = settings.GetValue(compName+"Et1JetCutR","");
            etString2 = settings.GetValue(compName+"Et2JetCutR","");
          }

          float eTCut1 = etString1.Atof();
          float eTCut2 = etString2.Atof();
          // std::cout << "Cut for " << myVarBuilder[iVar].varBuilderName << " with value of :" << varCut << " EtCut1 :" << eTCut1 << " EtCut2 : " << eTCut2 << '\n';


          float currEt1 = 0, currEt2 = 0, currMass1 = 0, currMass2 = 0;
          std::string etName = "Et", massName = "Mass";

          if(fabs(trigJets->at(matchedLdgIndex)->p4().Eta()) < 2.8){
            //if (selOffCounter >= 1 && firstOffJetIndex != -1) {
            myTools.p4Retriever(trigJets->at(matchedLdgIndex),etName,currEt1);
            myTools.p4Retriever(trigJets->at(matchedLdgIndex),massName,currMass1);
            myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(matchedLdgIndex), myVarBuilder[iVar].varBuilderName, f_var1, i_var1);
            //std::cout << "currEt1 : " << currEt1 << " etCut : " << eTCut1 << '\n';

            bool passTrigger = false, pass2Trigger = false;

            // if (selOffCounter >= 1 /*&& matchedLdgIndex != -1*/) {
            if (myVarBuilder[iVar].varBuilderType != "int")
            {
              if (myVarBuilder[iVar].varBuilderName == "eta" && currEt1 > eTCut1)
              {
                passTrigger = true;
              }
              else if (myVarBuilder[iVar].varBuilderName == "m" && currEt1 > eTCut1 && currMass1 > massCut) {
                passTrigger = true;
              }
              else if (currEt1 > eTCut1 && currMass1 > massCut && myTools.Comparator(incOri, f_var1, varCut)) //f_var1 < varCut)
              {
                passTrigger = true;
              }
            }
            else
            {
              int varIntCut = static_cast<int>(varCut);
              if (currEt1 > eTCut1 && currMass1 > massCut && myTools.Comparator(incOri, i_var1, varIntCut)) {
                passTrigger = true;
              }
            }
            myHistoVecEff.at(iVar).first->Fill(passTrigger, firstOffJet/1.e3);
            passTrigger = false;
            // }

            //if (selOffCounter > 1 && secondOffJetIndex != -1) {fabs(trigJets->at(matchedSubLdgIndex)->p4().Eta()) < 2.8
            if (selOffCounter > 1 /* && matchedSubLdgIndex != -1 && fabs(trigJets->at(matchedSubLdgIndex)->p4().Eta()) < 2.8*/) {
              int nJetsPassed = 0;
              for (size_t iJet = 0, length = trigJets->size(); iJet < length; iJet++) {
                myTools.p4Retriever(trigJets->at(iJet),etName,currEt2);
                myTools.p4Retriever(trigJets->at(iJet),massName,currMass2);
                myTools.ValueRetriever(myVarBuilder[iVar].varBuilderType, trigJets->at(iJet), myVarBuilder[iVar].varBuilderName, f_var2, i_var2);

                //bool passTrigger = false;

                if (myVarBuilder[iVar].varBuilderType != "int")
                {
                  if (myVarBuilder[iVar].varBuilderName == "eta" && currEt2 > eTCut2)
                  {
                    nJetsPassed++;
                    //pass2Trigger = true;
                  }
                  else if (myVarBuilder[iVar].varBuilderName == "m" && currEt2 > eTCut2 && currMass2 > massCut) {
                    nJetsPassed++;
                    //pass2Trigger = true;
                  }
                  else if (currEt2 > eTCut2 && currMass2 > massCut &&  myTools.Comparator(incOri, f_var2, varCut)) {
                    nJetsPassed++;
                    // pass2Trigger = true;
                  }
                }
                else
                {
                  int varIntCut = static_cast<int>(varCut);
                  if (currEt2 > eTCut2 && currMass2 > massCut && myTools.Comparator(incOri, i_var2, varIntCut)) {
                    nJetsPassed++;
                    // pass2Trigger = true;
                  }
                }
              }
              if (nJetsPassed >= 2){
                pass2Trigger = true;
              }
              nJetsPassed = 0;
              myHistoVecEff.at(iVar).second->Fill(pass2Trigger, secondOffJet/1.e3);
              pass2Trigger = false;
            }
          }
        }
      }

      //mark the end of the loop of entries
      if (iEntry == AvailableEntry - 1) {
        std::cout << "The end of the line" << '\n';
        stepTracker = stepTracker + 3;
      }
    }
  }
  std::cout << "Step tracker final : " << stepTracker << '\n';
  //mark the end of the loop of entries
  //stepTracker = stepTracker+1;

  if (stepTracker == mode) {
    std::cout << "breaking the while loop" << '\n';
    break;
  }

  // if (allPointsFound) {
  //   stepTracker = stepTracker + 1;
  // }
}

//----------------------------------------------------------------------
//Plotting section
//----------------------------------------------------------------------
//write the histogram to the root file
TFile* outFileHandle = new TFile(outRootFile.c_str(),"RECREATE");
outFileHandle->cd();

TCanvas* canvas = new TCanvas("canvas","canvas");
const TString outFile1 = outPdfFile;
canvas->Print(outFile1+"[");

// float entryWEta1 = 0, entryWEtaM1 = 0, entryWEtaMSS1 = 0;
// float entryWEta2 = 0, entryWEtaM2 = 0, entryWEtaMSS2 = 0;
//
// if (runOption.find(sigLost) != std::string::npos) {
//   entryWEta1 = myHistoVec.at(0).first->GetEntries();
//   entryWEtaM1 = myHistoVec.at(1).first->GetEntries();
//   entryWEtaM1 = (entryWEtaM1*100)/entryWEta1;
//   entryWEta2 = myHistoVec.at(0).second->GetEntries();
//   entryWEtaM2 = myHistoVec.at(1).second->GetEntries();
//   entryWEtaM2 = (entryWEtaM2*100)/entryWEta2;
//   std::cout << "signal remaining, eta+mass 1 jet cut :" << entryWEtaM1 << " | eta+mass 2 jet cut :" << entryWEtaM2 << '\n';
//   // myHistoVec.at(0).first->SetLineColor(kBlue);
//   // myHistoVec.at(1).first->SetLineColor(kRed);
//   // myHistoVec.at(0).first->Draw("");
//   // canvas->Update();
//   // canvas->Print(outFile1);
//   // canvas->Clear();
//   // myHistoVec.at(1).first->Draw("");
//   // gStyle->SetOptStat(0);
//   // canvas->Update();
//   // canvas->Print(outFile1);
//   // canvas->Clear();
//   // myHistoVec.at(0).second->SetLineColor(kBlue);
//   // myHistoVec.at(1).second->SetLineColor(kRed);
//   // myHistoVec.at(0).second->Draw("");
//   // myHistoVec.at(1).second->Draw("same");
//   // gStyle->SetOptStat(0);
//   // canvas->Update();
//   // canvas->Print(outFile1);
//   // canvas->Clear();
// }

for (size_t iVar = 0, length = myHistoVec.size(); iVar < length; iVar++){
  if (runOption.find(sigLost) != std::string::npos) {
    // if (iVar>1) {
    //   // entryWEta1 = myHistoVec.at(0).first->GetEntries();
    //   // entryWEta2 = myHistoVec.at(0).second->GetEntries();
    //   // entryWEtaM1 = myHistoVec.at(1).first->GetEntries();
    //   // entryWEtaM2 = myHistoVec.at(1).second->GetEntries();
    //   // entryWEtaMSS1 = myHistoVec.at(iVar).first->GetEntries();
    //   // entryWEtaMSS1 = (entryWEtaMSS1*100)/entryWEtaM1;
    //   // entryWEtaMSS2 = myHistoVec.at(iVar).second->GetEntries();
    //   // entryWEtaMSS2 = (entryWEtaMSS2*100)/entryWEtaM2;
    //   // std::cout << "signal remaining, eta+mass+" << myVarBuilder[iVar].varBuilderName << " 1 jet cut :" << entryWEtaMSS1 << " | eta+mass+" << myVarBuilder[iVar].varBuilderName << " 2 jet cut :" << entryWEtaMSS2 << '\n';
    //
    //   // myHistoVec.at(1).first->SetLineColor(kBlue);
    //   // myHistoVec.at(iVar).first->SetLineColor(kRed);
    //   // myHistoVec.at(1).first->Draw("");
    //   // myHistoVec.at(iVar).first->Draw("same");
    //   // gStyle->SetOptStat(0);
    //   // canvas->Update();
    //   // canvas->Print(outFile1);
    //   // canvas->Clear();
    //   // myHistoVec.at(1).second->SetLineColor(kBlue);
    //   // myHistoVec.at(iVar).second->SetLineColor(kRed);
    //   // myHistoVec.at(1).second->Draw("");
    //   // myHistoVec.at(iVar).second->Draw("same");
    //   myHistoVec.at(iVar).first->SetLineColor(kRed);
    //   myHistoVec.at(iVar).first->Draw("");
    //   //gStyle->SetOptStat(0);
    //   canvas->Update();
    //   canvas->Print(outFile1);
    //   canvas->Clear();
    // }
    myHistoCheck.at(iVar)->Write();
    //doesn't really matter, or will be empty because I reset the histogram above, fix it later
    //myHistoVec.at(iVar).first->Write();
    //myHistoVec.at(iVar).second->Write();
  }

  if (runOption.find(eqRate) != std::string::npos) {
    //std::cout << "Bin Content : " << BinContent << '\n';
    myHistoVecEqRate.at(iVar).first->Write();
    myHistoVecEqRate.at(iVar).second->Write();

    TLine *eqRateLine = new TLine(canvas->GetUxmin(), BinContent1, canvas->GetUxmax(), BinContent1);
    eqRateLine->SetLineStyle(2);
    eqRateLine->SetLineColor(kBlack);
    canvas->Update();
    //draw remaining histo
    myHistoVecEqRate.at(0).first->SetLineColor(kBlue);
    myHistoVecEqRate.at(0).second->SetLineColor(kRed);
    double numEntry = 0;
    numEntry = myHistoVecEqRate.at(0).first->GetBinContent(1) / 3;
    myHistoVecEqRate.at(0).first->GetYaxis()->SetRangeUser(0,numEntry);
    myHistoVecEqRate.at(0).first->Draw("");
    myHistoVecEqRate.at(0).second->Draw("same");
    myHistoVecEqRate.at(1).first->SetLineColor(kOrange);
    myHistoVecEqRate.at(1).second->SetLineColor(kBlack);
    myHistoVecEqRate.at(1).first->Draw("same");
    myHistoVecEqRate.at(1).second->Draw("same");
    myHistoVecEqRate.at(0).first->SetYTitle("Events Number");
    myHistoVecEqRate.at(0).first->SetXTitle("E_{T}  [GeV]");
    eqRateLine->Draw();
    gPad->Update();
    ATLASLabel(0.5,0.8,"Internal");

    gStyle->SetOptStat(0);
    canvas->Update();
    if (iVar>1) {
      myHistoVecEqRate.at(iVar).first->SetLineColor(kGreen);
      myHistoVecEqRate.at(iVar).second->SetLineColor(kMagenta);
      myHistoVecEqRate.at(iVar).first->Draw("same");
      myHistoVecEqRate.at(iVar).second->Draw("same");

      std::string aJetText = "HLT: 1 jet |#eta_{jet}|< 2.8, m > 30GeV, ";
      std::string JetsText = "HLT: 2 jets |#eta_{jet}|< 2.8, m > 30GeV, ";

      const char* aHistoText = myHistoVecEqRate.at(iVar).first->GetTitle();
      const char* twoHistoText = myHistoVecEqRate.at(iVar).second->GetTitle();

      aJetText +=  aHistoText;
      JetsText +=  twoHistoText;

      const char* oneJetText = aJetText.c_str();
      const char* twoJetText = JetsText.c_str();

      myBoxText(0.5, 0.75, 0.05, 1, 4, 1 , "HLT: 1 jet |#eta_{jet}|< 2.8");
      myBoxText(0.5, 0.70, 0.05, 1, 2, 1 , "HLT: 2 jets |#eta_{jet}|< 2.8");
      myBoxText(0.5, 0.65, 0.05, 1, 800, 1 , "HLT: 1 jet |#eta_{jet}|< 2.8, m > 30GeV");
      myBoxText(0.5, 0.60, 0.05, 1, 1, 1 , "HLT: 2 jets |#eta_{jet}|< 2.8, m > 30GeV ");

      myBoxText(0.5, 0.55, 0.05, 1, 3, 1 , oneJetText);
      myBoxText(0.5, 0.50, 0.05, 1, 6, 1 , twoJetText);
      myBoxText(0.5, 0.45, 0.05, 2, 1, 1 , "Equal rate line");

      eqRateLine->Draw();
      canvas->Update();
      canvas->Print(outFile1);
      canvas->Clear();
      myHistoVecEqRate.at(iVar).first->Delete();
      myHistoVecEqRate.at(iVar).second->Delete();
      delete eqRateLine;
    }
  }
}
//efficiency plot
if (runOption.find(trigEff) != std::string::npos) {
  for (size_t iVar = 0, length = ((myHistoVec.size()+1)/2); iVar < length; iVar++)
  {

    std::string aJetText = "";
    std::string JetsText = "";

    if (iVar<8) {
      myHistoVecEff.at(iVar*2).first->Write();
      myHistoVecEff.at(iVar*2).second->Write();
      myHistoVecEff.at(iVar*2+1).first->Write();
      myHistoVecEff.at(iVar*2+1).second->Write();

      if (iVar>0) {
        myHistoVecEff.at(0).first->SetLineColor(kBlue);
        myHistoVecEff.at(1).first->SetLineColor(kRed);
        myHistoVecEff.at(0).first->SetMarkerColor(kBlue);
        myHistoVecEff.at(1).first->SetMarkerColor(kRed);
        myHistoVecEff.at(0).first->Draw("");
        myHistoVecEff.at(1).first->Draw("same");
        if(outXAxis == "mass"){
          myHistoVecEff.at(0).first->SetTitle("my efficiency2;m leading jet (GeV);Efficiency");
        }
        else if (outXAxis == "pt"){
          myHistoVecEff.at(0).first->SetTitle("my efficiency2;pt leading jet (GeV);Efficiency");
        }
        gPad->Update();
        ATLASLabel(0.3,0.75,"Internal");
        gStyle->SetOptStat(0);
        canvas->Update();

        myHistoVecEff.at(iVar*2).first->SetLineColor(kGreen);
        myHistoVecEff.at(iVar*2+1).first->SetLineColor(kMagenta);
        myHistoVecEff.at(iVar*2).first->SetMarkerColor(kGreen);
        myHistoVecEff.at(iVar*2+1).first->SetMarkerColor(kMagenta);
        myHistoVecEff.at(iVar*2).first->Draw("same");
        myHistoVecEff.at(iVar*2+1).first->Draw("same");

        aJetText = "HLT: 1 jet |#eta_{jet}|< 2.8, m > 30GeV, ";
        JetsText = "HLT: 1 jet |#eta_{jet}|< 2.8, m > 30GeV, ";

        const char* aHistoText = myHistoVecEff.at(iVar*2).first->GetTitle();
        const char* twoHistoText = myHistoVecEff.at(iVar*2+1).first->GetTitle();

        aJetText +=  aHistoText;
        JetsText +=  twoHistoText;

        const char* oneJetText = aJetText.c_str();
        const char* twoJetText = JetsText.c_str();

        myBoxText(0.3, 0.70, 0.05, 1, 4, 1 , "HLT: 1 jet |#eta_{jet}|< 2.8");
        myBoxText(0.3, 0.65, 0.05, 1, 2, 1 , "HLT: 1 jet |#eta_{jet}|< 2.8, m > 30GeV, ");

        myBoxText(0.3, 0.60, 0.05, 1, 3, 1 , oneJetText);
        myBoxText(0.3, 0.55, 0.05, 1, 6, 1 , twoJetText);

        canvas->Update();
        canvas->Print(outFile1);
        canvas->Clear();
        canvas->Update();
        //myHistoVecEff.at(iVar*2).first->Delete();
        //myHistoVecEff.at(iVar*2+1).first->Delete();
      }

    }
    else if (iVar ==8){
      myHistoVecEff.at(iVar*2).first->Write();
      myHistoVecEff.at(iVar*2).second->Write();

      myHistoVecEff.at(0).first->SetLineColor(kBlue);
      myHistoVecEff.at(1).first->SetLineColor(kRed);
      myHistoVecEff.at(0).first->SetMarkerColor(kBlue);
      myHistoVecEff.at(1).first->SetMarkerColor(kRed);
      myHistoVecEff.at(0).first->Draw("");
      myHistoVecEff.at(1).first->Draw("same");
      if(outXAxis == "mass"){
        myHistoVecEff.at(0).first->SetTitle("my efficiency2;m leading jet (GeV);Efficiency");
      }
      else if (outXAxis == "pt"){
        myHistoVecEff.at(0).first->SetTitle("my efficiency2;pt leading jet (GeV);Efficiency");
      }
      gPad->Update();
      ATLASLabel(0.3,0.75,"Internal");
      gStyle->SetOptStat(0);
      canvas->Update();

      myHistoVecEff.at(iVar*2).first->SetLineColor(kGreen);
      myHistoVecEff.at(iVar*2).first->SetMarkerColor(kGreen);
      myHistoVecEff.at(iVar*2).first->Draw("same");

      const char* aHistoText = myHistoVecEff.at(iVar*2).first->GetTitle();

      aJetText +=  aHistoText;

      const char* oneJetText = aJetText.c_str();

      myBoxText(0.3, 0.70, 0.05, 1, 4, 1 , "HLT: 1 jet |#eta_{jet}|< 2.8");
      myBoxText(0.3, 0.65, 0.05, 1, 2, 1 , "HLT: 1 jet |#eta_{jet}|< 2.8, m > 30GeV, ");

      myBoxText(0.3, 0.60, 0.05, 1, 3, 1 , oneJetText);

      canvas->Update();
      canvas->Print(outFile1);
      canvas->Clear();
      canvas->Update();
      //myHistoVecEff.at(iVar*2).first->Delete();
    }


    if (iVar >0 && iVar<8) {

      myHistoVecEff.at(0).second->SetLineColor(kBlue);
      myHistoVecEff.at(1).second->SetLineColor(kRed);
      myHistoVecEff.at(0).second->SetMarkerColor(kBlue);
      myHistoVecEff.at(1).second->SetMarkerColor(kRed);
      myHistoVecEff.at(0).second->Draw("");
      myHistoVecEff.at(1).second->Draw("same");

      if(outXAxis == "mass"){
        myHistoVecEff.at(0).second->SetTitle("my efficiency2;m sub-leading jet (GeV);Efficiency");
      }
      else if (outXAxis == "pt"){
        myHistoVecEff.at(0).second->SetTitle("my efficiency2;pt sub-leading jet (GeV);Efficiency");
      }
      gPad->Update();
      ATLASLabel(0.3,0.75,"Internal");

      gStyle->SetOptStat(0);
      canvas->Update();
        myHistoVecEff.at(iVar*2).second->SetLineColor(kGreen);
        myHistoVecEff.at(iVar*2+1).second->SetLineColor(kMagenta);
        myHistoVecEff.at(iVar*2).second->SetMarkerColor(kGreen);
        myHistoVecEff.at(iVar*2+1).second->SetMarkerColor(kMagenta);
        myHistoVecEff.at(iVar*2).second->Draw("same");
        myHistoVecEff.at(iVar*2+1).second->Draw("same");

        aJetText = "HLT: 2 jet |#eta_{jet}|< 2.8, m > 30GeV, ";
        JetsText = "HLT: 2 jet |#eta_{jet}|< 2.8, m > 30GeV, ";

        const char* aHistoText = myHistoVecEff.at(iVar*2).second->GetTitle();
        const char* twoHistoText = myHistoVecEff.at(iVar*2+1).second->GetTitle();

        aJetText +=  aHistoText;
        JetsText +=  twoHistoText;

        const char* oneJetText = aJetText.c_str();
        const char* twoJetText = JetsText.c_str();

        myBoxText(0.3, 0.70, 0.05, 1, 4, 1 , "HLT: 2 jets |#eta_{jet}|< 2.8");
        myBoxText(0.3, 0.65, 0.05, 1, 2, 1 , "HLT: 2 jets |#eta_{jet}|< 2.8, m > 30GeV, ");

        myBoxText(0.3, 0.60, 0.05, 1, 3, 1 , oneJetText);
        myBoxText(0.3, 0.55, 0.05, 1, 6, 1 , twoJetText);

        canvas->Update();
        canvas->Print(outFile1);
        canvas->Clear();
        canvas->Update();
        //myHistoVecEff.at(iVar*2).second->Delete();
        //myHistoVecEff.at(iVar*2+1).second->Delete();

    }
    else if (iVar ==8){
      myHistoVecEff.at(0).second->SetLineColor(kBlue);
      myHistoVecEff.at(1).second->SetLineColor(kRed);
      myHistoVecEff.at(0).second->SetMarkerColor(kBlue);
      myHistoVecEff.at(1).second->SetMarkerColor(kRed);
      myHistoVecEff.at(0).second->Draw("");
      myHistoVecEff.at(1).second->Draw("same");

      if(outXAxis == "mass"){
        myHistoVecEff.at(0).second->SetTitle("my efficiency2;m sub-leading jet (GeV);Efficiency");
      }
      else if (outXAxis == "pt"){
        myHistoVecEff.at(0).second->SetTitle("my efficiency2;pt sub-leading jet (GeV);Efficiency");
      }
      gPad->Update();
      ATLASLabel(0.3,0.75,"Internal");

      gStyle->SetOptStat(0);
      canvas->Update();
      myHistoVecEff.at(iVar*2).second->SetLineColor(kGreen);
      myHistoVecEff.at(iVar*2).second->SetMarkerColor(kGreen);
      myHistoVecEff.at(iVar*2).second->Draw("same");

      const char* aHistoText = myHistoVecEff.at(iVar*2).second->GetTitle();
      aJetText = "";
      aJetText +=  aHistoText;

      const char* oneJetText = aJetText.c_str();

      myBoxText(0.3, 0.70, 0.05, 1, 4, 1 , "HLT: 2 jets |#eta_{jet}|< 2.8");
      myBoxText(0.3, 0.65, 0.05, 1, 2, 1 , "HLT: 2 jets |#eta_{jet}|< 2.8, m > 30GeV, ");

      myBoxText(0.3, 0.60, 0.05, 1, 3, 1 , oneJetText);

      canvas->Update();
      canvas->Print(outFile1);
      canvas->Clear();
      canvas->Update();
      //myHistoVecEff.at(iVar*2).second->Delete();
    }
  }
}

canvas->Print(outFile1+"]");
return 0;
}
