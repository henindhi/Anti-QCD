#!/bin/sh

sigCalc="/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/SigCalc.cxx"
#subFile="/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/"
slurm="/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/sbAntiQCD.sh"

for i in {12..17}
do
  echo $i
  sed -i "s/x00/x${i}/g" $sigCalc
  sed -i "s/x00/x${i}/g" $slurm
  sbatch $slurm
  sed -i "s/x${i}/x00/g" $sigCalc
  sed -i "s/x${i}/x00/g" $slurm
  #sleep 2
done
