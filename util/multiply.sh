#!/bin/bash

cd /afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/
#MyVVjjTrigger/share/
#input="/beegfs/users/henindhi/MyVVjjTrigger/source/myVVjjtrig.sh"
prefix="zprime"
input="/afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/submitAntiQCD_${prefix}.sh"
#input="/beegfs/users/henindhi/MyVVjjTrigger/source/MyVVjjTrigger/share/MyVVjjTriggerAlgJobOptions.py"
#prefix="x010"
#outFile="myJETM99output."$prefix".pool.root"

#echo $outFile

#while IFS='' read -r line || [[ -n "$line" ]]; do
#    echo "Text read from file: $line"
#done < $inputDir$prefix

#for i in $(seq -f "%03g" 1 14)
for i in {15..17}
do
  echo $i
  sed -i "s/x00/x${i}/g" $input
  #scp $input "MyVVjjTriggerAlgJobOptions_"$i".py"
  scp $input "submitAntiQCD_${prefix}_x"$i".sh"
  #sbatch $input
  sed -i "s/x${i}/x00/g" $input
  #sleep 2
done
#paste -s -d ' ' $input"x"$i | tee $input"x"$i
#paste -s -d ' ' $input"x005" | tee $input"x005"
