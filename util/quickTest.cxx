//C++ standard library
#include <vector>
#include <iostream>
#include <string>
//local dependencies
#include "Anti_QCD/ConfigParser.h"
#include "Anti_QCD/SimpleTools.h"
#include "Anti_QCD/HistoBuilder.h"
//#include "Anti_QCD/HCTriggerBase.h"
#include "Anti_QCD/EnergyCorrelatorRatiosCalc.h"
#include "Anti_QCD/NSubjettinessRatiosCalc.h"
//AtlasStyle
#include "AtlasStyle.C"
#include "AtlasUtils.C"
#include "AtlasLabels.C"
//ROOT includes
#include "TString.h"
#include "TChain.h"
#include "TFile.h"
#include "TEfficiency.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLine.h"
#include "TNamed.h"
#include "TH1F.h"
#include "THStack.h"

int main (int argc, char* argv[])
{
  SetAtlasStyle();
  xAOD::TEvent event;
  // Check arguments
  if (argc < 3)
    {
      printf("USAGE: %s <input root file> <output pdf file>\n",argv[0]);
      return 1;
    }

  const char* inRootFile1 = argv[1];
  const char* inRootFile2 = argv[2];
  std::string outPdfFile = argv[3];
  TFile* outFileHandle1 = TFile::Open(inRootFile1);
  TFile* outFileHandle2 = TFile::Open(inRootFile2);

  TCanvas* canvas = new TCanvas("canvas","canvas");
  const TString outFile1 = outPdfFile;
  canvas->Print(outFile1+"[");

  TH1F *h1 = (TH1F*)outFileHandle1->Get("Width_check_higgs");
  TH1F *h2 = (TH1F*)outFileHandle2->Get("MyAntiKt10TrimofflineLCW_calibrated.Width");

  h1->SetLineColor(kBlue);
  h2->SetLineColor(kRed);

  THStack hs("hs","test stacked histograms");
  hs.Add(h1);
  hs.Add(h2);
  hs.Draw("nostack");
  hs.GetXaxis()->SetTitle("Arbirtary");
  hs.GetYaxis()->SetTitle("Arbritary number");
  canvas->Update();

  ATLASLabel(0.6,0.75,"Internal");
  gStyle->SetOptStat(0);

  canvas->Update();
  canvas->Print(outFile1);
  canvas->Print(outFile1+"]");
  return 0;
}
