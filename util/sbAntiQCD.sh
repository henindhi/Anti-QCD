#!/bin/sh
#SBATCH -J x01_antiQCD
#SBATCH -e /beegfs/users/henindhi/antiQCDOutput/logs/compilationTest_x01_L.e
#SBATCH -o /beegfs/users/henindhi/antiQCDOutput/logs/compilationTest_x01_L.o
#SBATCH -c 1
#SBATCH -p rhel6-short

srun /afs/cern.ch/user/h/henindhi/public/Anti_QCD/Anti_QCD/util/submitAntiQCD_higgs_x01.sh
