#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

cd /afs/cern.ch/user/h/henindhi/public/Anti_QCD/
rcSetup
rc find_packages
rc compile

#m-asspo:1000,1500
#j-etcut:1J10,2J10,2J20

date="0322"
sample="higgs"
axis="pt"
dir="left"
pref="x05"

cd /beegfs/users/henindhi/antiQCDOutput/

#sigLost
SigCalc ${date}_${sample}_${pref}_sigLost_${dir}.root ${date}_${sample}_${pref}_sigLost_${dir}.pdf ${axis} sigLost ${sample} yes /beegfs/users/henindhi/mcForQT/hh_bbbb_c10_M1500.JETM6/DAOD_JETM6.12307634._000001.pool.root.1

#eqRate
SigCalc ${date}_${sample}_${pref}_eqRate_${dir}.root ${date}_${sample}_${pref}_eqRate_${dir}.pdf ${axis} eqRate data no /beegfs/users/henindhi/JETM99Deriv/21.2-jettriggerderivation/run/17Repro/*

#trigEff
SigCalc ${date}_${sample}_masspo_jetcut_trigEff_${dir}.root ${date}_${sample}_${pref}_trigEff_${dir}.pdf ${axis} trigEff data no /beegfs/users/henindhi/JETM99Deriv/21.2-jettriggerderivation/run/17Repro/*
